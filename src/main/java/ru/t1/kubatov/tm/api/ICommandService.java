package ru.t1.kubatov.tm.api;

import ru.t1.kubatov.tm.model.Command;

public interface ICommandService {

    Command[] getTerminalCommands();

}
