package ru.t1.kubatov.tm.component;

import ru.t1.kubatov.tm.api.IBootstrap;
import ru.t1.kubatov.tm.api.ICommandController;
import ru.t1.kubatov.tm.api.ICommandRepository;
import ru.t1.kubatov.tm.api.ICommandService;
import ru.t1.kubatov.tm.constant.ArgumentConstant;
import ru.t1.kubatov.tm.constant.CommandConstant;
import ru.t1.kubatov.tm.controller.CommandController;
import ru.t1.kubatov.tm.repository.CommandRepository;
import ru.t1.kubatov.tm.service.CommandService;

import java.util.Scanner;

public final class Bootstrap implements IBootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    @Override
    public void run(final String[] args) {
        parseArguments(args);
        parseCommands();
    }

    private void parseArguments(final String[] args) {
        if (args == null || args.length == 0) return;
        final String arg = args[0];
        parseArgument(arg);
    }

    private void parseCommands() {
        commandController.showWelcome();
        final Scanner scanner = new Scanner(System.in);
        while (!Thread.currentThread().isInterrupted()) {
            System.out.println("ENTER COMMAND:");
            final String command = scanner.next();
            parseCommand(command);
        }
    }

    private void parseCommand(final String command) {
        if (command == null || command.isEmpty()) return;
        switch (command) {
            case CommandConstant.VERSION:
                commandController.showVersion();
                break;
            case CommandConstant.HELP:
                commandController.showHelp();
                break;
            case CommandConstant.INFO:
                commandController.showDeveloperInfo();
                break;
            case CommandConstant.EXIT:
                exit();
                break;
            case CommandConstant.COMMANDS:
                commandController.showCommands();
                break;
            case CommandConstant.ARGUMENTS:
                commandController.showArguments();
                break;
            default:
                commandController.showCommandError();
        }
    }

    private void parseArgument(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case ArgumentConstant.VERSION:
                commandController.showVersion();
                break;
            case ArgumentConstant.HELP:
                commandController.showHelp();
                break;
            case ArgumentConstant.INFO:
                commandController.showDeveloperInfo();
                break;
            case ArgumentConstant.COMMANDS:
                commandController.showCommands();
                break;
            case ArgumentConstant.ARGUMENTS:
                commandController.showArguments();
                break;
            default:
                commandController.showArgumentError();
        }
        exit();
    }

    private void exit() {
        System.exit(0);
    }

}
