package ru.t1.kubatov.tm.api;

public interface IBootstrap {

    void run(String[] args);

}
