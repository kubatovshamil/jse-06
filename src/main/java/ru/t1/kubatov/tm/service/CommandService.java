package ru.t1.kubatov.tm.service;

import ru.t1.kubatov.tm.api.ICommandRepository;
import ru.t1.kubatov.tm.api.ICommandService;
import ru.t1.kubatov.tm.model.Command;

public class CommandService implements ICommandService {

    private ICommandRepository commandRepository;

    public CommandService(final ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public Command[] getTerminalCommands() {
        return commandRepository.getTerminalCommands();
    }

}
