package ru.t1.kubatov.tm.repository;

import ru.t1.kubatov.tm.api.ICommandRepository;
import ru.t1.kubatov.tm.model.Command;
import ru.t1.kubatov.tm.constant.CommandConstant;
import ru.t1.kubatov.tm.constant.ArgumentConstant;

public final class CommandRepository implements ICommandRepository {

    private static final Command VERSION = new Command(
            CommandConstant.VERSION, ArgumentConstant.VERSION, "Show application version."
    );

    private static final Command HELP = new Command(
            CommandConstant.HELP, ArgumentConstant.HELP, "Show application commands."
    );

    private static final Command INFO = new Command(
            CommandConstant.INFO, ArgumentConstant.INFO, "Show developer info."
    );

    private static final Command COMMANDS = new Command(
            CommandConstant.COMMANDS, ArgumentConstant.COMMANDS, "Show application commands."
    );

    private static final Command ARGUMENTS = new Command(
            CommandConstant.ARGUMENTS, ArgumentConstant.ARGUMENTS, "Show application arguments."
    );

    private static final Command EXIT = new Command(
            CommandConstant.EXIT, null, "Close application."
    );

    private static final Command[] COMMANDS_ARRAY = new Command[]{
            VERSION, HELP, INFO, COMMANDS, ARGUMENTS, EXIT
    };

    @Override
    public Command[] getTerminalCommands() {
        return COMMANDS_ARRAY;
    }

}
